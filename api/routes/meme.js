const mongoose = require("mongoose");
const router = require("express").Router();

const checkAuth = require("../middleware/check-auth");
const Image = require("../models/image");

router.get("/:memeId", async (req, res, next) => {
  const memeId = req.params.memeId;
  const meme = await Image.findOne({ _id: memeId });
  res.status(200).json(meme);
});

router.delete("/:memeId", async (req, res) => {
  const memeId = req.params.memeId;
  await Image.remove({ _id: memeId })
    .then((result) => {
      res.status(200).json({
        message: "Image deleted",
      });
    })
    .catch((err) => {
      // console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;

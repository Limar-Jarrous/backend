const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const express = require('express');
const bcrypt = require('bcrypt');

const router = express.Router();

const User = require('../models/user');
const Image = require('../models/image');
const checkAuth = require('../middleware/check-auth');

router.post('/signup', (req, res, next) => {
    const { email, password, userName } = req.body;
    User.findOne({ email: email })
        .exec()
        .then(user => {
            if (user) {
                return res.status(409).json({
                    message: 'Mail exists'
                })
            } else {
                bcrypt.hash(password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: email,
                            password: hash,
                            userName: userName,
                            savedImages: []
                        });
                        user.save()
                            .then(result => {
                                console.log(result);
                                res.status(201).json({
                                    message: 'User created'
                                })
                            })
                            .catch(err => {
                                // console.log(err);
                                res.status(500).json({
                                    error: err
                                })
                            });
                    }
                })
            }
        })
    // .catch(er => console.log('errr', err))
});

router.post('/login', (req, res, next) => {
    // const { email, password, userName } = req.body;
    User.findOne({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'Auth failed'
                });
            }
            bcrypt.compare(req.body.password, user.password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
                if (result) {
                    // console.log(`${process.env.JWT_KEY}`);
                    const token = jwt.sign({
                        email: user.email,
                        id: user._id
                    },
                        process.env.JWT_KEY,
                        { expiresIn: "1h" }
                    )
                    return res.status(200).json({
                        message: 'Auth successful',
                        token: token,
                        email: user.email,
                        userName: user.userName,
                        userId: user._id
                    });
                }
                res.status(401).json({
                    message: 'Auth failed'
                });
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
})

router.get('/all', async (req, res) => {
    const getUser = await User.find();
    res.status(200).json(getUser);
});

router.get('/saved/:userId', checkAuth, async (req, res, next) => {
    const allImages = await Image.find();
    console.log(allImages)
    const userId = req.params.userId;
    const getUser = User.find({ _id: userId }).exec()
        .then(user => {
            // console.log('user: ', user);
            const savedIds = user[0].savedImages;
            // console.log('savedIds: ', savedIds);
            const savedImages = allImages.filter(image => savedIds.includes(image._id));
            // console.log('savedImages: ', savedImages);
            res.status(200).json(savedImages);
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        });
    // allImages.map()
})

router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId;
    User.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'User deleted'
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

module.exports = router;
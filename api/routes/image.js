const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

const checkAuth = require("../middleware/check-auth");
const Image = require("../models/image");

router.post("/upload", async (req, res) => {
  const { imageURL, uploadedByUserId, uploadedByUserName } = req.body;
  try {
    const newImage = new Image({
      _id: mongoose.Types.ObjectId(),
      imageURL: imageURL,
      uploadedByUserName: uploadedByUserName,
      uploadedByUserId: uploadedByUserId,
      // likes: req.body.likes
    });
    await newImage.save();
    res.status(201).json(newImage);
  } catch (err) {
    console.error("Something went wrong", err);
    res.status(500).json({
      error: err,
    });
  }
});

router.get("/all", async (req, res) => {
  const getImage = await Image.find();
  res.status(200).json(getImage);
});

router.get("/last", async (req, res) => {
  const getImage = await Image.findOne().sort({ _id: -1 });
  res.status(200).json(getImage);
});

router.get("/:userId", async (req, res, next) => {
  const userId = req.params.userId;
  const userImages = await Image.find({ uploadedByUserId: userId });
  res.status(200).json(userImages);
});

module.exports = router;

const mongoose = require('mongoose');

const imageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    imageURL: { type: String, required: true },
    uploadedByUserName: { type: String, required: true },
    uploadedByUserId: { type: String, required: true },
    likes: { type: Number, default: 0 }
});

module.exports = mongoose.model('Image', imageSchema);
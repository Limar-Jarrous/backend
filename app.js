const mongoose = require("mongoose");
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
// const dotenv = require('dotenv');

const app = express();

const userRoutes = require("./api/routes/user");
const imgRoutes = require("./api/routes/image");
const memeRoutes = require("./api/routes/meme");

const db =
  "mongodb+srv://user0:" +
  process.env.MONGO_PASSWORD +
  "@cluster1.vib84.mongodb.net/Cluster1?retryWrites=true&w=majority";

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("Connected to MongoDB ...");
  })
  .catch((err) => {
    console.error("Could not Connect to MongoDb !!!", err);
  });
// mongoose.Promise = global.Promise;

app.use(morgan("dev"));
app.use(cors());
app.use(express.json());

// Handling CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH");
    return res.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/memes", imgRoutes);
app.use("/meme", memeRoutes);
app.use("/user", userRoutes);

app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
